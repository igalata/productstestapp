package com.android.igalata.productstestapp;

public class Const {
    public static final String URL = "https://openapi.etsy.com/v2/";
    public static final String ETSY_KEY = "y1ksy8rnznttfthaxq6w0zfr";
    public static final int CACHE_SIZE = 10 * 1024 * 1024;

    public static final String DATABASE_NAME = "Products";
    public static final String TABLE_PRODUCTS = "Products";
    public static final String TABLE_CATEGORIES = "Categories";
    public static final String ID = "id";
    public static final String TITLE = "title";
    public static final String DESCRIPTION = "desc";
    public static final String IMAGE_URL = "image_url";
    public static final String PRICE = "price";
    public static final String CATEGORY_NAME = "category_name";
    public static final String CATEGORY_SHORT_NAME = "short_name";
}
