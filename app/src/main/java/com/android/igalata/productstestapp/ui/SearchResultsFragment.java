package com.android.igalata.productstestapp.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.igalata.productstestapp.R;
import com.android.igalata.productstestapp.api.ApiHelper;
import com.android.igalata.productstestapp.model.Product;
import com.android.igalata.productstestapp.ui.utils.UiUtils;

import java.util.List;

import butterknife.ButterKnife;

public class SearchResultsFragment extends BaseProductListFragment {
    private String mCategory;
    private String mQuery;
    private RecyclerView.OnScrollListener mOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            onListScrolled();
        }
    };

    public static SearchResultsFragment newInstance(String category, String query) {
        SearchResultsFragment fragment = new SearchResultsFragment();
        fragment.setArguments(getArguments(category, query));
        return fragment;
    }

    private static Bundle getArguments(String category, String query) {
        Bundle arguments = new Bundle();
        arguments.putString("category", category);
        arguments.putString("query", query);
        return arguments;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments != null) {
            mCategory = arguments.getString("category");
            mQuery = arguments.getString("query");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_products_list, container, false);
        ButterKnife.bind(this, view);
        mActivity = (MainActivity) getActivity();
        UiUtils.showProgressDialog(mActivity);
        initRefreshLayout();
        setLayoutManager();
        return view;
    }

    private void getResults(int offset) {
        if (mAdapter == null || !mAdapter.isLoading()) {
            startLoading();
            ApiHelper.getInstance(mActivity).getSearchResults(mCategory, mQuery, offset, this::initList, error -> onError());
        }
    }

    @Override
    protected void refresh() {
        mAdapter = null;
        getResults(0);
    }

    @Override
    protected void showProductInfo(Product product) {
        ProductInfoFragment fragment = ProductInfoFragment.newInstance(product);
        mActivity.changeFragment(fragment, true);
    }

    private void initList(List<Product> products) {
        stopLoading();
        if (mAdapter == null) {
            setAdapter(products);
        } else {
            updateAdapter(products);
        }
        mRecyclerView.addOnScrollListener(mOnScrollListener);
    }

    private void onListScrolled() {
        if (isNeedToLoadMore()) {
            mRefreshLayout.setRefreshing(true);
            getResults(mAdapter.getItemCount());
        }
    }

    private boolean isNeedToLoadMore() {
        GridLayoutManager layoutManager = (GridLayoutManager) mRecyclerView.getLayoutManager();
        int totalItemCount = layoutManager.getItemCount();
        int lastVisibleItem = layoutManager.findLastVisibleItemPosition();
        return (!mAdapter.isLoading() && totalItemCount <= (lastVisibleItem + 6) && mAdapter.getItemCount() < mAdapter.getTotalCount());
    }

    private void updateAdapter(List<Product> products) {
        mAdapter.add(products);
        mAdapter.notifyDataSetChanged();
    }

    private void onError() {
        stopLoading();
    }

    private void stopLoading() {
        if (mAdapter != null) {
            mAdapter.setLoading(false);
        }
        UiUtils.hideProgressDialog();
        mRefreshLayout.setRefreshing(false);
    }

    private void startLoading() {
        if (mAdapter != null) {
            mAdapter.setLoading(true);
        }
    }
}
