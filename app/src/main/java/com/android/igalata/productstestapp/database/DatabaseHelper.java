package com.android.igalata.productstestapp.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.android.igalata.productstestapp.Const;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static DatabaseHelper mInstance;

    private DatabaseHelper(Context context) {
        super(context, Const.DATABASE_NAME, null, 1);
    }

    public static DatabaseHelper getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new DatabaseHelper(context);
        }

        return mInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "CREATE TABLE " + Const.TABLE_PRODUCTS + "("
                        + Const.ID + " INTEGER PRIMARY KEY, "
                        + Const.TITLE + " TEXT, "
                        + Const.DESCRIPTION + " TEXT, "
                        + Const.PRICE + " TEXT, "
                        + Const.IMAGE_URL + " TEXT)"
        );
        db.execSQL(
                "CREATE TABLE " + Const.TABLE_CATEGORIES + "("
                        + Const.ID + " INTEGER PRIMARY KEY, "
                        + Const.CATEGORY_NAME + " TEXT, "
                        + Const.CATEGORY_SHORT_NAME + " TEXT)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Const.TABLE_PRODUCTS);
        db.execSQL("DROP TABLE IF EXISTS " + Const.TABLE_CATEGORIES);
        onCreate(db);
    }
}
