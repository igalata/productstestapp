package com.android.igalata.productstestapp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.android.igalata.productstestapp.R;
import com.android.igalata.productstestapp.model.Category;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SpinnerAdapter extends ArrayAdapter<Category> {
    private List<Category> mCategories;

    public SpinnerAdapter(Context context, int resource, List<Category> objects) {
        super(context, resource, objects);
        mCategories = objects;
    }

    @Override
    public Category getItem(int position) {
        return mCategories.get(position);
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (view == null) {
            view = LayoutInflater.from(getContext()).inflate(R.layout.spinner_item, parent, false);
            createViewHolder(view);
        }

        bindViewHolder(view, position);
        return view;
    }

    private void createViewHolder(View view) {
        ViewHolder viewHolder = new ViewHolder();
        ButterKnife.bind(viewHolder, view);
        view.setTag(viewHolder);
    }

    private void bindViewHolder(View view, int position) {
        ViewHolder viewHolder = (ViewHolder) view.getTag();
        viewHolder.categoryName.setText(getItem(position).getShortName());
    }

    class ViewHolder {
        @Bind(R.id.category_name)
        TextView categoryName;
    }
}
