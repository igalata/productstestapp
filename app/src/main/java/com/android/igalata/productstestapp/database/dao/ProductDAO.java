package com.android.igalata.productstestapp.database.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.android.igalata.productstestapp.Const;
import com.android.igalata.productstestapp.database.DatabaseHelper;
import com.android.igalata.productstestapp.model.Image;
import com.android.igalata.productstestapp.model.Product;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ProductDAO {
    private DatabaseHelper mHelper;

    public ProductDAO(Context context) {
        mHelper = DatabaseHelper.getInstance(context);
    }

    public void createProduct(Product product) {
        SQLiteDatabase database = mHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Const.TITLE, product.getTitle());
        contentValues.put(Const.DESCRIPTION, product.getDescription());
        contentValues.put(Const.PRICE, product.getPrice());
        contentValues.put(Const.IMAGE_URL, product.getImages().get(0).getFullImageUrl());
        contentValues.put(Const.ID, product.getId());
        database.insert(Const.TABLE_PRODUCTS, null, contentValues);
    }

    public void removeProduct(int productId) {
        SQLiteDatabase database = mHelper.getWritableDatabase();
        database.delete(Const.TABLE_PRODUCTS, Const.ID + " = " + productId, null);
    }

    public List<Product> readAllProducts() {
        SQLiteDatabase database = mHelper.getReadableDatabase();
        Cursor cursor = database.rawQuery("SELECT * FROM " + Const.TABLE_PRODUCTS, null);
        List<Product> products = new ArrayList<>();

        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); ++i) {
            Product product = new Product();
            product.setId(cursor.getInt(cursor.getColumnIndex(Const.ID)));
            product.setTitle(cursor.getString(cursor.getColumnIndex(Const.TITLE)));
            product.setDescription(cursor.getString(cursor.getColumnIndex(Const.DESCRIPTION)));
            product.setPrice(cursor.getString(cursor.getColumnIndex(Const.PRICE)));
            product.setImages(Arrays.asList(new Image(cursor.getString(cursor.getColumnIndex(Const.IMAGE_URL)))));
            products.add(product);
            cursor.moveToNext();
        }

        cursor.close();
        return products;
    }

    public boolean containsProduct(int productId) {
        SQLiteDatabase database = mHelper.getReadableDatabase();
        Cursor cursor = database.rawQuery("SELECT * FROM " + Const.TABLE_PRODUCTS + " WHERE " + Const.ID + " = " + productId, null);
        boolean contains = cursor.getCount() > 0;
        cursor.close();
        return contains;
    }
}
