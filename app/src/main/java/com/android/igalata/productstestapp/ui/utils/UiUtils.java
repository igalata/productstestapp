package com.android.igalata.productstestapp.ui.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.View;

import com.android.igalata.productstestapp.R;

public class UiUtils {
    private static ProgressDialog mProgressDialog;

    public static void showProgressDialog(Context context) {
        hideProgressDialog();
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setMessage(context.getText(R.string.text_loading));
        mProgressDialog.show();
    }

    public static void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }

    public static void showShackbar(View view, int textRes) {
        if (view != null) {
            Snackbar.make(view, textRes, Snackbar.LENGTH_LONG).show();
        }
    }
}
