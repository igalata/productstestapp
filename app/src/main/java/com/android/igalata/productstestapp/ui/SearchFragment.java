package com.android.igalata.productstestapp.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;

import com.android.igalata.productstestapp.R;
import com.android.igalata.productstestapp.api.ApiHelper;
import com.android.igalata.productstestapp.database.dao.CategoryDAO;
import com.android.igalata.productstestapp.model.Category;
import com.android.igalata.productstestapp.ui.adapter.SpinnerAdapter;
import com.android.igalata.productstestapp.ui.utils.UiUtils;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchFragment extends Fragment {
    @Bind(R.id.spinner_category)
    Spinner mCategorySpinner;
    @Bind(R.id.search_field)
    EditText mSearchField;
    private MainActivity mActivity;
    private CategoryDAO mCategoryDao;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        ButterKnife.bind(this, view);
        mActivity = (MainActivity) getActivity();
        mCategoryDao = new CategoryDAO(mActivity);
        getCategories();
        return view;
    }

    private void getCategories() {
        List<Category> categories = mCategoryDao.readAllCategories();
        if (categories.isEmpty()) {
            UiUtils.showProgressDialog(mActivity);
            ApiHelper.getInstance(mActivity).getCategories(this::onResponse, error -> onError());
        } else {
            setAdapter(categories);
        }
    }

    private void onResponse(List<Category> categories) {
        UiUtils.hideProgressDialog();
        mCategoryDao.createCategories(categories);
        setAdapter(categories);
    }

    private void setAdapter(List<Category> categories) {
        SpinnerAdapter adapter = new SpinnerAdapter(mActivity, R.layout.spinner_item, categories);
        mCategorySpinner.setAdapter(adapter);
    }

    private void onError() {
        UiUtils.hideProgressDialog();
        UiUtils.showShackbar(getView(), R.string.error_request_failed);
    }

    @OnClick(R.id.button_submit)
    public void onClickSubmit() {
        if (!TextUtils.isEmpty(mSearchField.getText())) {
            showResults();
        } else {
            UiUtils.showShackbar(getView(), R.string.error_enter_query);
        }
    }

    private void showResults() {
        mActivity.changeFragment(SearchResultsFragment.newInstance(getSelectedCategory(), getQuery()), true);
    }

    private String getSelectedCategory() {
        return ((Category) mCategorySpinner.getSelectedItem()).getCategoryName();
    }

    private String getQuery() {
        return mSearchField.getText().toString();
    }
}
