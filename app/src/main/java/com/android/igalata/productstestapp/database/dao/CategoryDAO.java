package com.android.igalata.productstestapp.database.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.android.igalata.productstestapp.Const;
import com.android.igalata.productstestapp.database.DatabaseHelper;
import com.android.igalata.productstestapp.model.Category;

import java.util.ArrayList;
import java.util.List;

public class CategoryDAO {
    private DatabaseHelper mHelper;

    public CategoryDAO(Context context) {
        mHelper = DatabaseHelper.getInstance(context);
    }

    public void createCategories(List<Category> categories) {
        for (int i = 0; i < categories.size(); ++i) {
            createCategory(categories.get(i));
        }
    }

    private void createCategory(Category category) {
        SQLiteDatabase database = mHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Const.CATEGORY_NAME, category.getCategoryName());
        contentValues.put(Const.CATEGORY_SHORT_NAME, category.getShortName());
        database.insert(Const.TABLE_CATEGORIES, null, contentValues);
    }

    public List<Category> readAllCategories() {
        SQLiteDatabase database = mHelper.getReadableDatabase();
        Cursor cursor = database.rawQuery("SELECT * FROM " + Const.TABLE_CATEGORIES, null);
        List<Category> categories = new ArrayList<>();

        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); ++i) {
            Category category = new Category();
            category.setCategoryName(cursor.getString(cursor.getColumnIndex(Const.CATEGORY_NAME)));
            category.setShortName(cursor.getString(cursor.getColumnIndex(Const.CATEGORY_SHORT_NAME)));
            categories.add(category);
            cursor.moveToNext();
        }

        cursor.close();
        return categories;
    }
}
