package com.android.igalata.productstestapp.model;

import com.google.gson.annotations.SerializedName;

public class Image extends BaseModel {
    @SerializedName("url_fullxfull")
    private String fullImageUrl;

    public Image() {
    }

    public Image(String fullImageUrl) {
        this.fullImageUrl = fullImageUrl;
    }

    public String getFullImageUrl() {
        return fullImageUrl;
    }

    public void setFullImageUrl(String fullImageUrl) {
        this.fullImageUrl = fullImageUrl;
    }
}
