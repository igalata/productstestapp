package com.android.igalata.productstestapp.model;

import com.google.gson.annotations.SerializedName;

public class Category extends BaseModel {
    @SerializedName("category_name")
    private String categoryName;
    @SerializedName("short_name")
    private String shortName;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    @Override
    public String toString() {
        return shortName;
    }
}
