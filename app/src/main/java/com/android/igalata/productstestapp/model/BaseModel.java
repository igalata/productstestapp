package com.android.igalata.productstestapp.model;

import java.io.Serializable;

public abstract class BaseModel implements Serializable {
    private int totalCount;

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }
}
