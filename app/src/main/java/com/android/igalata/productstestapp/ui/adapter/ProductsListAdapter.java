package com.android.igalata.productstestapp.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.igalata.productstestapp.Const;
import com.android.igalata.productstestapp.R;
import com.android.igalata.productstestapp.api.ApiHelper;
import com.android.igalata.productstestapp.cache.ProductImageCache;
import com.android.igalata.productstestapp.model.Image;
import com.android.igalata.productstestapp.model.Product;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ProductsListAdapter extends RecyclerView.Adapter<ProductsListAdapter.ProductViewHolder> {

    private List<Product> mProducts;
    private Context mContext;
    private ImageLoader mLoader;
    private boolean mIsLoading;
    private int mTotalCount;
    private OnProductClickListener mListener;

    public ProductsListAdapter(Context context, List<Product> products, OnProductClickListener listener) {
        mContext = context;
        mProducts = products;
        ProductImageCache cache = new ProductImageCache(Const.CACHE_SIZE);
        mLoader = new ImageLoader(ApiHelper.getRequestQueue(), cache);
        setHasStableIds(true);
        mTotalCount = getItemCount() == 0 ? 0 : mProducts.get(0).getTotalCount();
        mListener = listener;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_product, parent, false);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        holder.bind(mProducts.get(position), position);
    }

    private void loadImage(ProductViewHolder viewHolder, List<Image> images, int position) {
        mProducts.get(position).setImages(images);
        String imageUrl = images.get(0).getFullImageUrl();
        viewHolder.image.setImageUrl(imageUrl, mLoader);
    }

    public void add(List<Product> products) {
        mProducts.addAll(products);
        notifyDataSetChanged();
    }

    public void add(Product product) {
        mProducts.add(product);
        notifyDataSetChanged();
    }

    private void onError() {

    }

    public void remove(Product product) {
        mProducts.remove(product);
        notifyDataSetChanged();
    }

    public int getTotalCount() {
        return mTotalCount;
    }

    @Override
    public int getItemCount() {
        return mProducts.size();
    }

    public boolean isLoading() {
        return mIsLoading;
    }

    public void setLoading(boolean isLoading) {
        this.mIsLoading = isLoading;
    }

    public interface OnProductClickListener {
        void onClick(Product product);
    }

    class ProductViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.image)
        NetworkImageView image;
        @Bind(R.id.title)
        TextView title;

        public ProductViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(v -> mListener.onClick(mProducts.get(getAdapterPosition())));
        }

        public void bind(Product product, int position) {
            title.setText(Html.fromHtml(product.getTitle()));
            if (product.getImages() == null) {
                ApiHelper.getInstance(mContext).getImages(product.getId(), images -> loadImage(this, images, position), error -> onError());
            } else {
                loadImage(this, product.getImages(), position);
            }
        }
    }
}
