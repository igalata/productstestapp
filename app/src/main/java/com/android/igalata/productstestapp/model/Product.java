package com.android.igalata.productstestapp.model;

import android.text.Html;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Product extends BaseModel {
    @SerializedName("listing_id")
    private int id;
    private String title;
    private String description;
    private String price;
    private List<Image> images;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return Html.fromHtml(title).toString();
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return Html.fromHtml(description).toString();
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }
}
