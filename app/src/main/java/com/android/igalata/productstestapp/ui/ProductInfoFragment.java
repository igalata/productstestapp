package com.android.igalata.productstestapp.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.igalata.productstestapp.Const;
import com.android.igalata.productstestapp.R;
import com.android.igalata.productstestapp.api.ApiHelper;
import com.android.igalata.productstestapp.cache.ProductImageCache;
import com.android.igalata.productstestapp.database.dao.ProductDAO;
import com.android.igalata.productstestapp.model.Product;
import com.android.igalata.productstestapp.ui.utils.UiUtils;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProductInfoFragment extends Fragment {
    @Bind(R.id.image)
    NetworkImageView mImage;
    @Bind(R.id.title)
    TextView mTitle;
    @Bind(R.id.description)
    TextView mDescription;
    @Bind(R.id.price)
    TextView mPrice;
    @Bind(R.id.button_save)
    Button mButtonSave;
    @Bind(R.id.button_remove)
    Button mButtonRemove;
    private Product mProduct;
    private ProductDAO mProductDao;
    private OnProductRemovedListener mOnProductRemovedListener;
    private OnProductSavedListener mOnProductSavedListener;

    public static ProductInfoFragment newInstance(Product product) {
        ProductInfoFragment fragment = new ProductInfoFragment();
        setArguments(fragment, product);
        return fragment;
    }

    private static void setArguments(ProductInfoFragment fragment, Product product) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("product", product);
        fragment.setArguments(bundle);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mProduct = (Product) getArguments().getSerializable("product");
        mProductDao = new ProductDAO(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product_info, container, false);
        ButterKnife.bind(this, view);
        initViews();
        return view;
    }

    private void initViews() {
        ImageLoader imageLoader = new ImageLoader(ApiHelper.getRequestQueue(), new ProductImageCache(Const.CACHE_SIZE));
        mImage.setImageUrl(mProduct.getImages().get(0).getFullImageUrl(), imageLoader);
        mTitle.setText(mProduct.getTitle());
        mDescription.setText(mProduct.getDescription());
        mPrice.setText(mProduct.getPrice() + "$");
        setButtonsVisibility(mProductDao.containsProduct(mProduct.getId()));
    }

    @OnClick(R.id.button_save)
    void onClickSave() {
        mProductDao.createProduct(mProduct);
        setButtonsVisibility(true);
        if (mOnProductSavedListener != null) {
            mOnProductSavedListener.onProductSaved(mProduct);
        }
        UiUtils.showShackbar(getView(), R.string.text_product_saved);
    }

    @OnClick(R.id.button_remove)
    void onClickRemove() {
        mProductDao.removeProduct(mProduct.getId());
        setButtonsVisibility(false);
        if (mOnProductRemovedListener != null) {
            mOnProductRemovedListener.onProductRemoved(mProduct);
        }
        UiUtils.showShackbar(getView(), R.string.text_product_removed);
    }

    private void setButtonsVisibility(boolean isSaved) {
        mButtonSave.setVisibility(isSaved ? View.GONE : View.VISIBLE);
        mButtonRemove.setVisibility(isSaved ? View.VISIBLE : View.GONE);
    }

    public void setOnProductRemovedListener(OnProductRemovedListener listener) {
        mOnProductRemovedListener = listener;
    }

    public void setOnProductSavedListener(OnProductSavedListener listener) {
        mOnProductSavedListener = listener;
    }

    public interface OnProductRemovedListener {
        void onProductRemoved(Product product);
    }

    public interface OnProductSavedListener {
        void onProductSaved(Product product);
    }
}
