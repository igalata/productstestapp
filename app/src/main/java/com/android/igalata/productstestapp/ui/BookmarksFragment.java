package com.android.igalata.productstestapp.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.igalata.productstestapp.R;
import com.android.igalata.productstestapp.database.dao.ProductDAO;
import com.android.igalata.productstestapp.model.Product;

import butterknife.ButterKnife;

public class BookmarksFragment extends BaseProductListFragment {
    private ProductDAO mProductDao;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_products_list, container, false);
        ButterKnife.bind(this, view);
        mActivity = (MainActivity) getActivity();
        mProductDao = new ProductDAO(mActivity);
        setLayoutManager();
        initRefreshLayout();
        return view;
    }

    @Override
    protected void refresh() {
        setAdapter(mProductDao.readAllProducts());
        mRefreshLayout.setRefreshing(false);
    }

    @Override
    protected void showProductInfo(Product product) {
        ProductInfoFragment fragment = ProductInfoFragment.newInstance(product);
        fragment.setOnProductRemovedListener(mAdapter::remove);
        fragment.setOnProductSavedListener(mAdapter::add);
        mActivity.changeFragment(fragment, true);
    }
}
