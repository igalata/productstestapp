package com.android.igalata.productstestapp.api;

import android.content.Context;

import com.android.igalata.productstestapp.Const;
import com.android.igalata.productstestapp.model.Category;
import com.android.igalata.productstestapp.model.Image;
import com.android.igalata.productstestapp.model.Product;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import java.util.List;

public class ApiHelper {
    private static ApiHelper mInstance;
    private static RequestQueue mRequestQueue;

    public static ApiHelper getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new ApiHelper();
            mRequestQueue = Volley.newRequestQueue(context);
        }

        return mInstance;
    }

    public static RequestQueue getRequestQueue() {
        return mRequestQueue;
    }

    public void getCategories(Response.Listener<List<Category>> listener, Response.ErrorListener errorListener) {
        String endpoint = Const.URL + "taxonomy/categories?api_key=" + Const.ETSY_KEY;
        GetRequest<Category> request = new GetRequest<>(Category.class, Request.Method.GET, endpoint, "", listener, errorListener);
        mRequestQueue.add(request);
    }

    public void getSearchResults(String category, String query, int offset, Response.Listener<List<Product>> listener, Response.ErrorListener errorListener) {
        String endpoint = Const.URL + "listings/active?api_key=" + Const.ETSY_KEY
                + "&category=" + category + "&keywords=" + query + "&offset=" + offset + "&limit=30";
        GetRequest<Product> request = new GetRequest<>(Product.class, Request.Method.GET, endpoint, "", listener, errorListener);
        mRequestQueue.add(request);
    }

    public void getImages(int productId, Response.Listener<List<Image>> listener, Response.ErrorListener errorListener) {
        String endpoint = Const.URL + "listings/" + productId + "/images?api_key=" + Const.ETSY_KEY;
        GetRequest<Image> request = new GetRequest<>(Image.class, Request.Method.GET, endpoint, "", listener, errorListener);
        mRequestQueue.add(request);
    }
}
