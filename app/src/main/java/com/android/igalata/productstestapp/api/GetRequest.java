package com.android.igalata.productstestapp.api;

import com.android.igalata.productstestapp.model.BaseModel;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class GetRequest<T extends BaseModel> extends JsonRequest<List<T>> {
    private Class<T> mClass;

    public GetRequest(Class<T> type, int method, String url, String requestBody,
                      Response.Listener<List<T>> listener, Response.ErrorListener errorListener) {
        super(method, url, requestBody, listener, errorListener);
        mClass = type;
    }

    @Override
    protected Response<List<T>> parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            return Response.success(getItemsFromJson(jsonString), HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JSONException e) {
            return Response.error(new ParseError(e));
        }
    }

    private List<T> getItemsFromJson(String json) throws JSONException {
        JSONArray itemsArray = new JSONObject(json).getJSONArray("results");
        List<T> items = new ArrayList<>();
        int totalCount = new JSONObject(json).getInt("count");
        Gson gson = new Gson();
        for (int i = 0; i < itemsArray.length(); ++i) {
            T item = gson.fromJson(itemsArray.get(i).toString(), mClass);
            item.setTotalCount(totalCount);
            items.add(item);
        }

        return items;
    }
}
