package com.android.igalata.productstestapp.ui;

import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.android.igalata.productstestapp.R;
import com.android.igalata.productstestapp.model.Product;
import com.android.igalata.productstestapp.ui.adapter.ProductsListAdapter;

import java.util.List;

import butterknife.Bind;

public abstract class BaseProductListFragment extends Fragment {
    protected MainActivity mActivity;
    protected ProductsListAdapter mAdapter;
    @Bind(R.id.recycler_view)
    RecyclerView mRecyclerView;
    @Bind(R.id.refresh_layout)
    SwipeRefreshLayout mRefreshLayout;

    @Override
    public void onResume() {
        super.onResume();
        refresh();
    }

    protected abstract void refresh();

    protected void initRefreshLayout() {
        mRefreshLayout.setOnRefreshListener(this::refresh);
        mRefreshLayout.setColorSchemeColors(mActivity.getResources().getColor(R.color.colorPrimary),
                mActivity.getResources().getColor(R.color.colorAccent));
    }

    protected void setAdapter(List<Product> products) {
        mAdapter = new ProductsListAdapter(mActivity, products, this::showProductInfo);
        mRecyclerView.setAdapter(mAdapter);
    }

    protected abstract void showProductInfo(Product product);

    protected void setLayoutManager() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mActivity, 2);
        mRecyclerView.setLayoutManager(gridLayoutManager);
    }
}
